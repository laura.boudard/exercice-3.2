# Interfaces Graphiques, Clients lourds et Injection
 ++ Une application fenêtrée avec Swing, des patterns comme MVC, ou encore IOC avec l'injection Spring

## Contexte
* Un premier investisseur s'est intéressé au projet, et il souhaite pouvoir consulter rapidement un prototype sur un appareil quel qu'il soit.
* Nous leur proposons de générer un petit programme s'exécutant sur un de nos PCs pour une démo
* Pour ce faire, nous allons développer un nouveau module, badge-gui, se reposant sur badges-wallet comme dépendance, et y exploiter la technologie Spring...
* Par ailleurs, notre équipe va s'installer dans une ferme d'entreprises où les postes de travail sont tous déjà préconfigurés. Le hic, c'est que l'on a aucun droit pour installer quoi que ce soit, et le seul IDE accessible est IntelliJ en version communautaire. Ceux qui sont en télétravail vont devoir s'aligner sur cet outil en l'installant et en l'utilisant. Ce qui est plutôt plaisant, c'est que cela semble être plutôt efficace, au moins autant qu'éclipse en tout cas...


## Objectifs
* Mises en application:
 - [x] (Exercice 1) Démarrage en 2 temps, affichage rapide de notre liste de métadonnées de Badge, puis customisation fine de la liste ...
    - [x] Commencer, avec un tutoriel, à afficher simplement la liste des badges (métadonnées), en s'aidant d'une classe d'introspection fournie
    - [x] Implémenter plus sérieusement un modèle de tableau avec des comportements d'affichage des cellules/lignes selon certains critères.   
 - [x] (Exercice 2) Permettre l'affichage d'un badge, lors d'un évènement de double-clic sur une ligne
 - [ ] (Exercice 3) Permettre l'ajout d'un badge et la mise à jour visuellement du tableau à l'aide du pattern Observable/Observer
 - [ ] (Exercice 4) Appliquer le pattern IOC avec Spring pour injecter le DAO, et appliquer ce pattern également sur le projet badges-wallet 
----

## Une Première interface graphique Java avec Swing

### Ecran d'affichage d'un Badge

Nous voulons donc afficher l'image d'un badge de notre liste, lorsque l'on double-cick... 
 
 - [ ] Dans cette optique, nous allons commencer par structurer un peu nos classes, selon ce diagramme:

  ```plantuml
     @startuml
   
   title __BADGES-GUI's Class Diagram__\n
   
     namespace fr.cnam.foad.nfa035.badges.gui {
       class fr.cnam.foad.nfa035.badges.gui.BadgeWalletGUI {
       }
     }
     
   
     namespace fr.cnam.foad.nfa035.badges.gui {
       namespace components {
         class fr.cnam.foad.nfa035.badges.gui.components.BadgePanel {
         }
       }
     }
     
   
     namespace fr.cnam.foad.nfa035.badges.gui {
       namespace model {
         class fr.cnam.foad.nfa035.badges.gui.model.BadgesModel {
         }
       }
     }
     
     namespace fr.cnam.foad.nfa035.badges.gui {
       namespace renderer {
         class fr.cnam.foad.nfa035.badges.gui.renderer.BadgeSizeCellRenderer {
         }
       }
     }
     
   
     namespace fr.cnam.foad.nfa035.badges.gui {
       namespace renderer {
         class fr.cnam.foad.nfa035.badges.gui.renderer.DefaultBadgeCellRenderer {
         }
       }
     }
   
   
     namespace fr.cnam.foad.nfa035.badges.gui {
       class fr.cnam.foad.nfa035.badges.gui.BadgeWalletGUI {
           ~ tableList : List<DigitalBadge>
           {static} - RESOURCES_PATH : String
           - button1 : JButton
           - panelBas : JPanel
           - panelHaut : JPanel
           - panelImageContainer : JPanel
           - panelParent : JPanel
           - scrollBas : JScrollPane
           - scrollHaut : JScrollPane
           - table1 : JTable
           + BadgeWalletGUI()
           + getIcon()
           {static} + main()
           - createUIComponents()
           - setCreatedUIFields()
       }
     }
     
   
     fr.cnam.foad.nfa035.badges.gui.BadgeWalletGUI o-- fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge : badge
     fr.cnam.foad.nfa035.badges.gui.BadgeWalletGUI o-- fr.cnam.foad.nfa035.badges.gui.components.BadgePanel : badgePanel
     fr.cnam.foad.nfa035.badges.gui.BadgeWalletGUI o-- fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO : dao
     fr.cnam.foad.nfa035.badges.gui.components.BadgePanel -up-|> javax.swing.JPanel
     fr.cnam.foad.nfa035.badges.gui.components.BadgePanel o-- fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge : badge
     fr.cnam.foad.nfa035.badges.gui.components.BadgePanel o-- fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO : dao
     fr.cnam.foad.nfa035.badges.gui.model.BadgesModel -up-|> javax.swing.table.AbstractTableModel
     fr.cnam.foad.nfa035.badges.gui.renderer.BadgeSizeCellRenderer -up-|> javax.swing.table.DefaultTableCellRenderer
     fr.cnam.foad.nfa035.badges.gui.renderer.DefaultBadgeCellRenderer -up-|> javax.swing.table.DefaultTableCellRenderer
   
   
     namespace fr.cnam.foad.nfa035.badges.gui {
       namespace components {
         class fr.cnam.foad.nfa035.badges.gui.components.BadgePanel {
             + BadgePanel()
             # paintComponent()
         }
       }
     }
   
   
     namespace fr.cnam.foad.nfa035.badges.gui {
       namespace model {
         class fr.cnam.foad.nfa035.badges.gui.model.BadgesModel {
             - badges : List<DigitalBadge>
             - entetes : String[]
             {static} - serialVersionUID : long
             + BadgesModel()
             + getColumnClass()
             + getColumnCount()
             + getColumnName()
             + getRowCount()
             + getValueAt()
         }
       }
     }
   
     namespace fr.cnam.foad.nfa035.badges.gui {
       namespace renderer {
         class fr.cnam.foad.nfa035.badges.gui.renderer.BadgeSizeCellRenderer {
             ~ originBackground : Color
             ~ originForeground : Color
             + BadgeSizeCellRenderer()
             + getTableCellRendererComponent()
             - humanReadableByteCountBin()
         }
       }
     }
   
   
     namespace fr.cnam.foad.nfa035.badges.gui {
       namespace renderer {
         class fr.cnam.foad.nfa035.badges.gui.renderer.DefaultBadgeCellRenderer {
             ~ originForeground : Color
             + DefaultBadgeCellRenderer()
             + getTableCellRendererComponent()
         }
       }
     }
   right footer
   
   
   PlantUML diagram generated by SketchIt! (https://bitbucket.org/pmesmeur/sketch.it)
   For more information about this tool, please contact philippe.mesmeur@gmail.com
   endfooter
   
   @enduml
  ```

  - [ ] Ce qu'il nous reste à implémenter effectivement, il semblerait, c'est cette classe **BadgePanel** qui étend JPanel. Il s'agit cette fois d'un composant Custom, donc nous allons l'empaqueter dans un Panneau dont le cycle de vie est à désolidariser du moteur Forms. Pour faire celà, cochez la case **Custom create**:
    - ![Composant-custom][Composant-custom]
    - Cette méthode pourrait vous être utile:
    ```java
        protected void paintComponent(Graphics g) {
           super.paintComponent(g);
           ByteArrayOutputStream bos = new ByteArrayOutputStream();
           try {
               dao.getBadgeFromMetadata(bos, badge);
               g.drawImage(ImageIO.read(new ByteArrayInputStream(bos.toByteArray())), 0, 0, this.getWidth(), this.getHeight(), this);
           } catch (IOException ioException) {
               ioException.printStackTrace();
           }
       }
    ```
    - De même que cette belle ligne de code, à utiliser quelque part, probablement:
    ```java
    table1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                super.mouseClicked(event);
                if (event.getClickCount() == 2) {
                    int row = ((JTable) event.getComponent()).getSelectedRow();
                    panelHaut.removeAll();
                    panelHaut.revalidate();

                    badge = tableList.get(row);
                    setCreatedUIFields();

                    panelHaut.add(scrollHaut);
                    panelImageContainer.setPreferredSize(new Dimension(256, 256));
                    scrollHaut.setViewportView(panelImageContainer);

                    panelHaut.repaint();
                }
            }
        });
    ```
    
    - /!\ ceci-dit, à partir d'ici vous redéfinissez des objets, et là Intelli-J nous ennui avec des classes embarquées, notamment au niveau implémentation de LayoutManager. 
Donc à partir de là il est question de modifier l'attribut LayoutManager de chacun de vos JPanel et ainsi éviter des PullPointerException difficiles à corriger
Autrement il serait possible d'utiliser la méthode add(scrollHaut,new GridConstraints(...)) et non add(scrollHaut) mais alors là il nous faudrait introduire une dépendance vers une librairie intelliJ: on va donc éviter de trop dépendre de l'IDE.
    - Cf. https://stackoverflow.com/questions/9639017/intellij-gui-creator-jpanel-gives-runtime-null-pointer-exception-upon-adding-an/35431318
    - Donc mettre de préférence un GridBagLayout dans tous vos panel, ou bien tout autre layout non dépendant d'IntelliJ:
    - ![](screenshots/layout-non-intellij.png)

    - [ ] En suite, essayez de vérifier que le tout est cohérent, ordre des items, correspondance des images, aspect global...
    - [ ] Et voilà! Vous l'avez mérité, de contempler ce bel ouvrage fonctionnel (preuve en image avec votre code mais si possible):
    - ![Troisieme-IHM][Troisieme-IHM]
    


----

[Composant-custom]: screenshots/Composant-custom.png "Preuve 1"
[Troisieme-IHM]: screenshots/Troisieme-IHM.png "Preuve 2"

----

### Ressources

#### Coup de pouce pour le lancement :

![Coup-de-pouce](screenshots/demo-exo-3.2_partie1.mp4)


