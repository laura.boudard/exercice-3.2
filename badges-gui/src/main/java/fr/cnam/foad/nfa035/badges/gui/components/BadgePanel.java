package fr.cnam.foad.nfa035.badges.gui.components;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class BadgePanel extends JPanel {

    private DigitalBadge badge;

    DirectAccessBadgeWalletDAO dao;

    private static final String RESOURCES_PATH = "badges-gui/src/main/resources/";

    public BadgePanel(DirectAccessBadgeWalletDAO dao, DigitalBadge badge) {
        this.dao = dao;
        this.badge=badge;
    }

    public DigitalBadge getBadge() {return badge;}

    public void setBadge(DigitalBadge badge) {this.badge = badge;}

    public DirectAccessBadgeWalletDAO getDao() {return dao;}

    public void setDao(DirectAccessBadgeWalletDAO dao) {this.dao = dao;}



    /**
     * Permet de dessiner un composant
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            dao.getBadgeFromMetadata(bos, badge);
            g.drawImage(ImageIO.read(new ByteArrayInputStream(bos.toByteArray())), 0, 0, this.getWidth(), this.getHeight(), this);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

}
