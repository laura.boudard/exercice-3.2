package fr.cnam.foad.nfa035.badges.gui;

import fr.cnam.foad.nfa035.badges.gui.components.BadgePanel;
import fr.cnam.foad.nfa035.badges.gui.model.BadgesModel;
import fr.cnam.foad.nfa035.badges.gui.renderer.BadgeSizeCellRenderer;
import fr.cnam.foad.nfa035.badges.gui.renderer.DefaultBadgeCellRenderer;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.*;
import java.util.List;

/**
 * classe permettant la création de l'interface graphique et d'appliquer les modèles de customisation
 */
public class BadgeWalletGUI {
    private JButton button1;
    private JPanel panelParent;
    private JTable table1;
    private JScrollPane scrollBas;
    private JPanel panelBas;
    private JPanel panelHaut;
    private JScrollPane scrollHaut;
    private JPanel panelImageContainer;


    private static final String RESOURCES_PATH = "badges-gui/src/main/resources/";
    DirectAccessBadgeWalletDAO dao;
    private DigitalBadge badge;
    private BadgePanel panelBadge;
    List<DigitalBadge> tableList;

    public BadgeWalletGUI() {

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Bien joué!");
            }
        });

        try {
            // 1. Le Model
            dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");
            Set<DigitalBadge> metaSet = dao.getWalletMetadata();
            tableList = new ArrayList<>();
            tableList.addAll(metaSet);
            //TableModel tableModel = TableModelCreator.createTableModel(DigitalBadge.class, tableList);
            BadgesModel tableModel = new BadgesModel(tableList);

            // 2. Les Renderers...
            table1.setModel(tableModel);
            table1.getColumnModel().getColumn(4).setCellRenderer(new BadgeSizeCellRenderer());
            table1.setDefaultRenderer(Object.class, new DefaultBadgeCellRenderer());
            // Apparemment, les Objets Number sont traités à part, donc il faut le déclarer explicitement en plus de Object
            table1.setDefaultRenderer(Number.class, new DefaultBadgeCellRenderer());
            // Idem pour les Dates
            table1.setDefaultRenderer(Date.class, new DefaultBadgeCellRenderer());

            // 3. Tri initial
            table1.getRowSorter().toggleSortOrder(0);

            //permet de recevoir évènement de souris du composant
            table1.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent event) {
                    super.mouseClicked(event);
                    if (event.getClickCount() == 2) {
                        int row = ((JTable) event.getComponent()).getSelectedRow();
                        panelHaut.removeAll();
                        panelHaut.revalidate();

                        badge = tableList.get(row);
                        setCreatedUIFields();

                        panelHaut.add(scrollHaut);
                        scrollHaut.setViewportView(panelImageContainer);

                        panelHaut.repaint();
                    }
                }
            });

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    /**
     * code d'initilisation du panelImageContainer
     */
    private void setCreatedUIFields() {
        this.panelBadge = new BadgePanel(dao, badge);
        this.panelBadge.setPreferredSize(new Dimension(256,256));
        this.panelImageContainer = new JPanel();
        this.panelImageContainer.add(panelBadge);
    }

    /**
     * Commentez-moi
     * @param args les arguments
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame("My Badge Wallet");
        BadgeWalletGUI gui = new BadgeWalletGUI();
        frame.setContentPane(gui.panelParent);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setIconImage(gui.getIcon().getImage());
        frame.setVisible(true);
    }

    /**
     * Permet de modifier l'image de l'interface graphique
     * @return ImageIcon
     */
    public ImageIcon getIcon(){
        return new ImageIcon(getClass().getResource("/logo.png"));
    }

    /**
     * Permet d'instancier les ressources nécessaires d'initilisation du composant customisé
     */
    private void createUIComponents() {

        try {
            this.dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");
            //1.Le modèle
            Set<DigitalBadge> metaSet = new TreeSet<>(dao.getWalletMetadata());
            this.tableList = new ArrayList<>();
            tableList.addAll(metaSet);
            // Badge initial
            this.badge = tableList.get(0);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        this.setCreatedUIFields();

        }


    }

